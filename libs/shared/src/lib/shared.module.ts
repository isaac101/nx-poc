import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';

import { MaterialModule } from '@nx-ngrx-poc/material';
import { LoaderComponent } from './loader/loader.component';
import { NoteCardComponent } from './components/note-card/note-card.component';
import { NoteAddDialogComponent } from './components/note-add-dialog/note-add-dialog.component';

@NgModule({
  imports: [
    FlexLayoutModule,
    MaterialModule,
    CommonModule,
    ReactiveFormsModule,
  ],
  declarations: [LoaderComponent, NoteCardComponent, NoteAddDialogComponent],
  exports: [LoaderComponent, NoteCardComponent, NoteAddDialogComponent],
})
export class SharedModule {}
