import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, Inject } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-note-add-dialog',
  templateUrl: './note-add-dialog.component.html',
  styleUrls: ['./note-add-dialog.component.scss'],
})
export class NoteAddDialogComponent {
  data: any;
  type: any;
  noteForm: any;
  uploadedImg: any;

  constructor(
    public dialogRef: MatDialogRef<NoteAddDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public dialogData: any,
    private fb: FormBuilder
  ) {
    this.data = dialogData.data;
    this.type = dialogData.type;
    if (this.data.pic !== null && this.data.pic !== '') {
      this.uploadedImg = this.data.pic;
    }
    this.noteForm = this.fb.group({
      title: this.data.title,
      text: this.data.text,
    });
  }

  onConfirm(): void {
    if (this.uploadedImg && this.data.pic !== null) {
      this.dialogRef.close({
        ...this.noteForm.value,
        pic: this.uploadedImg,
      });
    } else if (this.noteForm.get('text').value && this.data.pic === null) {
      this.dialogRef.close(this.noteForm.value);
    } else {
      this.dialogRef.close(false);
    }
  }

  onDismiss(): void {
    this.dialogRef.close(false);
  }

  inputChange(data: any) {
    const file = data.files[0];
    const reader = new FileReader();

    reader.onloadend = () => {
      console.log('RESULT', reader.result);
      this.uploadedImg = reader.result;
    };

    reader.readAsDataURL(file);
  }
}
