export interface PicNote {
  title: string;
  text: string;
  pic: string;
}
