import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';

import { SharedModule } from '@nx-ngrx-poc/shared';
import { MaterialModule } from '@nx-ngrx-poc/material';
import { AboutRoutingModule } from './picture-notes.routing.module';
import { AboutComponent } from './picture-notes.component';

import { StoreModule } from '@ngrx/store';
import { reducers } from './store';

// Containers
import * as fromContainers from './containers';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    FlexLayoutModule,
    MaterialModule,
    AboutRoutingModule,
    SharedModule,
    StoreModule.forFeature('pic-notes', reducers),
  ],
  declarations: [AboutComponent, ...fromContainers.containers],
})
export class AboutModule {}
