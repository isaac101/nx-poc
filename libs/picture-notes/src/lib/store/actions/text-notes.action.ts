import { Action } from '@ngrx/store';

import { PicNote } from '../../models/picture-note.interface';

// load notes
export const SAVE_NOTE = '[Picture Note] Save Notes';

export class saveNotes implements Action {
  readonly type = SAVE_NOTE;
  constructor(public payload: PicNote) {}
}

export type NotesAction = saveNotes;
