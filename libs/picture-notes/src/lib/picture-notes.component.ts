import { Component, ChangeDetectionStrategy } from '@angular/core';

import { PicNote } from './models/picture-note.interface';
import { MatDialog } from '@angular/material/dialog';

import { NoteAddDialogComponent } from '@nx-ngrx-poc/shared';

import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import * as fromStore from './store';

@Component({
  selector: 'app-about',
  templateUrl: './picture-notes.component.html',
  styleUrls: ['./picture-notes.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AboutComponent {
  picNotes$: Observable<PicNote[]>;

  constructor(private dialog: MatDialog, private store: Store) {
    this.picNotes$ = this.store.select(fromStore.getAllNotes);
  }

  openForm() {
    const dialogData: any = {
      type: 'add',
      data: {
        title: '',
        text: '',
        pic: '',
      },
    };

    const dialogRef = this.dialog.open(NoteAddDialogComponent, {
      width: '450px',
      data: dialogData,
    });

    dialogRef.afterClosed().subscribe((dialogResult) => {
      console.log(dialogResult);
      if (dialogResult) {
        this.store.dispatch(new fromStore.saveNotes(dialogResult));
      }
    });
  }

  viewNote(note: PicNote) {
    const dialogData: any = {
      type: 'view',
      data: note,
    };

    const dialogRef = this.dialog.open(NoteAddDialogComponent, {
      width: '450px',
      data: dialogData,
    });

    dialogRef.afterClosed().subscribe((dialogResult) => {});
  }
}
