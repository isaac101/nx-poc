import { Component, Output, EventEmitter, Input, ChangeDetectionStrategy } from '@angular/core';

import { Observable } from 'rxjs';
import { PicNote } from '../../models/picture-note.interface';

@Component({
  selector: 'app-note-list',
  templateUrl: './pic-note-list.component.html',
  styleUrls: ['./pic-note-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PicNoteListComponent {
  @Input() notes: Observable<PicNote[]>;
  @Output() view = new EventEmitter<any>();

  constructor() {}

  viewNote(data: PicNote) {
    this.view.emit(data);
  }
}
