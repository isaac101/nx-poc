import { PicNoteListComponent } from './note-list/pic-note-list.component';
import { AddPicNoteComponent } from './add-note/pic-add-note.component';

export const containers: any[] = [PicNoteListComponent, AddPicNoteComponent];

export * from './note-list/pic-note-list.component';
export * from './add-note/pic-add-note.component';
