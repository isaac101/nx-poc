import { Component, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-add-note',
  templateUrl: './pic-add-note.component.html',
  styleUrls: ['./pic-add-note.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddPicNoteComponent {
  @Output() create = new EventEmitter<any>();

  constructor() {}

  addNote() {
    this.create.emit();
  }
}
