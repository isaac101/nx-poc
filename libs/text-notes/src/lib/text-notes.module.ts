import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';

import { SharedModule } from '@nx-ngrx-poc/shared';
import { MaterialModule } from '@nx-ngrx-poc/material';
import { HomeRoutingModule } from './text-notes-routing.module';
import { HomeComponent } from './text-notes.component';
// import { QuoteService } from './quote.service';

// Store
import { StoreModule } from '@ngrx/store';
// import { EffectsModule } from '@ngrx/effects';

import { reducers } from './store';

// Containers
import * as fromContainers from './containers';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    FlexLayoutModule,
    MaterialModule,
    HomeRoutingModule,
    StoreModule.forFeature('text-notes', reducers),
    // EffectsModule.forFeature(effects),
  ],
  declarations: [HomeComponent, ...fromContainers.containers],
})
export class HomeModule {}
