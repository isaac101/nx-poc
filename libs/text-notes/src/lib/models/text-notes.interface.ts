export interface TextNote {
  title: string;
  text: string;
}
