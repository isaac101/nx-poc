import { createSelector } from '@ngrx/store';

import * as fromFeature from '../reducers';
import * as fromNotes from '../reducers/text-notes.reducer';

export const getNoteState = createSelector(
  fromFeature.getNoteModuleState,
  (state: fromFeature.NoteModuleState) => state.notes
);

export const getNotesEntities = createSelector(getNoteState, fromNotes.getNoteEntities);

export const getAllNotes = createSelector(getNotesEntities, (entities) => {
  return Object.keys(entities).map((id) => entities[id]);
});
