import { ActionReducerMap, createFeatureSelector } from '@ngrx/store';

import * as fromNotes from './text-notes.reducer';

export interface NoteModuleState {
  notes: fromNotes.NoteState;
}

export const reducers: ActionReducerMap<NoteModuleState, any> = {
  notes: fromNotes.reducer,
};

export const getNoteModuleState =
  createFeatureSelector<NoteModuleState>('text-notes');
