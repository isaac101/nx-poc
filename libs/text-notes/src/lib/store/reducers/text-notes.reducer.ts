import * as fromNotes from '../actions/text-notes.action';

import { TextNote } from '../../models/text-notes.interface';
import { uuid } from 'uuidv4';

export interface NoteState {
  entities: { [id: number]: TextNote };
  count: number | undefined;
}

export const initialState: NoteState = {
  entities: {},
  count: undefined,
};

export function reducer(state = initialState, action: fromNotes.saveNotes): NoteState {
  switch (action.type) {
    case fromNotes.SAVE_NOTE: {
      const note = action.payload;
      const entities = {
        ...state.entities,
        ['id' + Math.random().toString(16).slice(2)]: note,
      };

      return {
        ...state,
        entities,
      };
    }

    default: {
      return state;
    }
  }
}

export const getNoteEntities = (state: NoteState) => state.entities;
