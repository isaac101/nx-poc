import { Action } from '@ngrx/store';

import { TextNote } from '../../models/text-notes.interface';

// load notes
export const SAVE_NOTE = '[Text Note] Save Notes';

export class saveNotes implements Action {
  readonly type = SAVE_NOTE;
  constructor(public payload: TextNote) {}
}

export type NotesAction = saveNotes;
