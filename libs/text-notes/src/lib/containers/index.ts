import { NoteListComponent } from './note-list/note-list.component';
import { AddNoteComponent } from './add-note/add-note.component';

export const containers: any[] = [NoteListComponent, AddNoteComponent];

export * from './note-list/note-list.component';
export * from './add-note/add-note.component';
