import { Component, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-add-note',
  templateUrl: './add-note.component.html',
  styleUrls: ['./add-note.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddNoteComponent {
  @Output() create = new EventEmitter<any>();

  constructor() {}

  addNote() {
    this.create.emit();
  }
}
