import { Component, Output, EventEmitter, Input, ChangeDetectionStrategy } from '@angular/core';

import { Observable } from 'rxjs';
import { TextNote } from '../../models/text-notes.interface';

@Component({
  selector: 'app-note-list',
  templateUrl: './note-list.component.html',
  styleUrls: ['./note-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NoteListComponent {
  @Input() notes: Observable<TextNote[]>;
  @Output() view = new EventEmitter<any>();

  constructor() {}

  viewNote(data: TextNote) {
    this.view.emit(data);
  }
}
