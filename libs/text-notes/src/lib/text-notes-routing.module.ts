import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// import { extract } from '@app/i18n';
import { HomeComponent } from './text-notes.component';
import { Shell } from '@nx-ngrx-poc/shell';

const routes: Routes = [
  Shell.childRoutes([
    { path: '', redirectTo: '/text-notes', pathMatch: 'full' },
    {
      path: 'text-notes',
      component: HomeComponent,
      data: { title: 'Text Notes' },
    },
  ]),
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [],
})
export class HomeRoutingModule {}
