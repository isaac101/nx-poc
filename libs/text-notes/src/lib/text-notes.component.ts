import { Component, ChangeDetectionStrategy } from '@angular/core';

import { TextNote } from './models/text-notes.interface';
import { MatDialog } from '@angular/material/dialog';

import { NoteAddDialogComponent } from '@nx-ngrx-poc/shared';

import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import * as fromStore from './store';

@Component({
  selector: 'app-text-notes',
  templateUrl: './text-notes.component.html',
  styleUrls: ['./text-notes.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeComponent {
  textNotes$: Observable<TextNote[]>;

  constructor(private dialog: MatDialog, private store: Store) {
    this.textNotes$ = this.store.select(fromStore.getAllNotes);
  }

  openForm() {
    const dialogData: any = {
      type: 'add',
      data: {
        title: '',
        text: '',
        pic: null,
      },
    };

    const dialogRef = this.dialog.open(NoteAddDialogComponent, {
      width: '350px',
      data: dialogData,
    });

    dialogRef.afterClosed().subscribe((dialogResult) => {
      console.log(dialogResult);
      if (dialogResult) {
        this.store.dispatch(new fromStore.saveNotes(dialogResult));
      }
    });
  }

  viewNote(note: TextNote) {
    const dialogData: any = {
      type: 'view',
      data: { ...note, pic: null },
    };

    const dialogRef = this.dialog.open(NoteAddDialogComponent, {
      width: '350px',
      data: dialogData,
    });

    dialogRef.afterClosed().subscribe((dialogResult) => {});
  }
}
